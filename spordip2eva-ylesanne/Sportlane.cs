﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spordip2eva_ylesanne
{
    class Sportlane
    {
        public string Nimi;
        public int Distants;
        public int Aeg;
        public double Kiirus => Distants * 1.0 / Aeg;
        public static Dictionary<string, Sportlane> Spordip2ev = new Dictionary<string, Sportlane>();
        //public Sportlane(string nimi) //kõikidel autodel peab olema numbrimärk
        ////{
        //    Nimi = nimi;// Numbrimärk on väli ja numbrivärk on parameeter. Kui sul on need sõnad erinevalt kirjutatud, siis saad this eest ära jätta, sest ta saab ise aru mis on mis. ''This'' on kirjutatud helelillana, mitte tumesinisena, järelikult optional.

        //    if (!Spordip2ev.ContainsKey(nimi)) Spordip2ev.Add(nimi, this);//Esimene lause ütleb kui EI OLE seda numbrit, teine lause, ALLES SIIS lisame dictionarysse. Sama numbrimärgi vältimine, kuna dictionarys ei tohi olla kaks korda sama võtmega asja- saad vea
        //}

        public override string ToString() //eriline string, ehk override.. muidu kui välja trükkida programmis, siis ta ütleb meil oma tüübi ja nime ainult.
        {
            return $"Sportlane :{Nimi} distants: {Distants} aeg:({Aeg}) ";
        }


    }
}
