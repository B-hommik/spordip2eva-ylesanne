﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace spordip2eva_ylesanne
{
    class Program
    {
        static void Main(string[] args)
        //{
        {
            string failiNimi = @"..\..\spordipäeva protokoll.txt";
            var protokoll = File.ReadAllLines(failiNimi)
                   .Skip(1)//jätame esimese rea lugemata kuna seal on tulbanimed
                           //.Where(x => x.Trim() != "")
                   .Select(x => x.Split(','))//siit tuleb massiiv
                   .Select(x => new { Nimi = x[0].Trim(), Distants = int.Parse(x[1].Trim()), Aeg = int.Parse(x[2].Trim()) })
                   .Select(x => new { Nimi = x.Nimi, Distants = x.Distants, Aeg = x.Aeg, Kiirus = x.Distants * 1.0 / x.Aeg }) //peaks saama ka ilma kiiruse vaelmita, kuna see on klassis olemas??? x.Distants * 1.0 / x.Aeg. SET TULEB KA MEISTERDADA SINNA.
                   .ToList();
            //kes on kõige kiirem sprinter, kõige lühem distants kõige kiiremini
            int lyhimdistants = protokoll.Min(x => x.Distants);
            int lyhimdistants2 = protokoll.Select(x => x.Distants).Min();
            int lyhimdistants3 = protokoll.Select(x => x.Distants).OrderBy(x => x).FirstOrDefault();
            double suurimkiirus = protokoll.Max(x => x.Kiirus);
            int pikimdistants = protokoll.Max(x => x.Distants);

            var kiireimSprinter =
                protokoll.Where(x => x.Distants == lyhimdistants)
                //.OrderByDescending(x => x.Kiirus)
                .OrderBy(x => x.Aeg)
                .FirstOrDefault();

            var kiireimJooksja =
                protokoll.Where(x => x.Kiirus == suurimkiirus)
                .OrderBy(x => x.Kiirus)
               .FirstOrDefault();

            var lyhikesedistantsiKiireim =
                protokoll.Where(x => x.Distants == lyhimdistants)
                //.OrderByDescending(x => x.Kiirus)
                .OrderBy(x => x.Aeg)
                .FirstOrDefault();

            var keskmisedistantsiKiireim =
                protokoll.Where(x => x.Distants > lyhimdistants)
                //.OrderByDescending(x => x.Kiirus)
                .OrderBy(x => x.Aeg)
                .FirstOrDefault();

            var pikadistantsiKiireim =
                protokoll.Where(x => x.Distants == pikimdistants)
                //.OrderByDescending(x => x.Kiirus)
                .OrderBy(x => x.Aeg)
                .FirstOrDefault();

            Console.WriteLine($"Kõige kiireim sprinter spordipäeval oli {kiireimSprinter.Nimi}, ta läbis {kiireimSprinter.Distants}m kiirusega {kiireimSprinter.Kiirus.ToString("F0")}m/s.");
            Console.WriteLine($"Kõige kiireim jooksja spordipäeval oli {kiireimJooksja.Nimi}, ta läbis {kiireimJooksja.Distants}m kiirusega {kiireimJooksja.Kiirus.ToString("F0")}m/s.");
            Console.WriteLine($"Kõige kiireim lühikese distantsi jooksja spordipäeval oli {lyhikesedistantsiKiireim.Nimi}, ta läbis {lyhikesedistantsiKiireim.Distants}m ajaga {lyhikesedistantsiKiireim.Aeg} sekundit.");
            Console.WriteLine($"Kõige kiireim 200m distantsi jooksja spordipäeval oli {keskmisedistantsiKiireim.Nimi}, ta läbis {keskmisedistantsiKiireim.Distants}m ajaga {keskmisedistantsiKiireim.Aeg} sekundit.");
            Console.WriteLine($"Kõige kiireim 400m distantsi jooksja spordipäeval oli {pikadistantsiKiireim.Nimi}, ta läbis {pikadistantsiKiireim.Distants}m ajaga {pikadistantsiKiireim.Aeg} sekundit.");
        }
        }
    }
